import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { AboutUsRoutingModule } from './aboutus-routing.module';
import { AboutUsComponent } from './aboutus.component';

@NgModule({
  imports: [CommonModule, TranslateModule, AboutUsRoutingModule],
  declarations: [AboutUsComponent]
})
export class AboutUsModule {}
