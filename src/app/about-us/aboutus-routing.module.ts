import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract } from '@app/core';
import { Shell } from '@app/shell/shell.service';
import { AboutUsComponent } from './aboutus.component';

const routes: Routes = [
  Shell.childRoutes([{ path: 'about-us', component: AboutUsComponent, data: { title: extract('About Us') } }])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class AboutUsRoutingModule {}
