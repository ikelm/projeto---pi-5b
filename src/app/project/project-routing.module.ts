import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract } from '@app/core';
import { Shell } from '@app/shell/shell.service';
import { ProjectComponent } from './project.component';

const routes: Routes = [
  Shell.childRoutes([{ path: 'project', component: ProjectComponent, data: { title: extract('Project') } }])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class ProjectRoutingModule {}
