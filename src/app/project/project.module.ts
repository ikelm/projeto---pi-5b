import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { ProjectRoutingModule } from './project-routing.module';
import { ProjectComponent } from './project.component';

@NgModule({
  imports: [CommonModule, TranslateModule, ProjectRoutingModule],
  declarations: [ProjectComponent]
})
export class ProjectModule {}
